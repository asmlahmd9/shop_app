import 'package:flutter/material.dart';

const defaultColor = Color(0xff188780);

Color myColor = const Color(0xff188780);
MaterialColor defaultColorAppMaterial = MaterialColor(myColor.value, {
  50: myColor.withOpacity(0.1),
  100: myColor.withOpacity(0.2),
  200: myColor.withOpacity(0.3),
  300: myColor.withOpacity(0.4),
  400: myColor.withOpacity(0.5),
  500: myColor.withOpacity(0.6),
  600: myColor.withOpacity(0.7),
  700: myColor.withOpacity(0.8),
  800: myColor.withOpacity(0.9),
  900: myColor.withOpacity(1.0),
});
